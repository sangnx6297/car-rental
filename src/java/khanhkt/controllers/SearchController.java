/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.controllers;

import com.mysql.jdbc.StringUtils;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import khanhkt.cars.CarDAO;
import khanhkt.categories.CategoriesDAO;
import khanhkt.categories.CategoriesDTO;
import khanhkt.utils.MyConstants;
import khanhkt.utils.Utils;

/**
 *
 * @author nhm95
 */
public class SearchController extends HttpServlet {

    private final String SEARCH_PAGE = "listCar.jsp";
    private final String PAGINATION_PARAM = "page";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = SEARCH_PAGE;

        int page = Integer.parseInt(StringUtils.isNullOrEmpty(request.getParameter(PAGINATION_PARAM)) ? "0" : request.getParameter(PAGINATION_PARAM));
        String name = request.getParameter("carName");
        String categoryId = request.getParameter("carCategory");
        String rentalDateFrom = request.getParameter("rentalDateFrom");
        String rentalDateTo = request.getParameter("rentalDateTo");

        System.out.println("Query Params: " + request.getQueryString());
        if (page < 1) {
            page = 1;
        }

        CarDAO car = new CarDAO();
        CategoriesDAO categoryDAO = new CategoriesDAO();
        try {
            car.getAll(name, categoryId, rentalDateFrom, rentalDateTo, MyConstants.LIMIT_PAGINATE, Utils.convertPageToOfsset(MyConstants.LIMIT_PAGINATE, page));
            car.getTotal(name, categoryId, rentalDateFrom, rentalDateTo);
            categoryDAO.getAll();
            List<CategoriesDTO> listCategory = categoryDAO.getList();

            request.setAttribute("carName", name);
            request.setAttribute("carCategory", categoryId);
            request.setAttribute("rentalDateFrom", rentalDateFrom);
            request.setAttribute("rentalDateTo", rentalDateTo);

            request.setAttribute("carDAO", car);
            request.setAttribute("listCategory", listCategory);
            request.setAttribute("list", car.list);
            request.setAttribute("pagination", Utils.generatePagination(request.getQueryString(), car.total, MyConstants.LIMIT_PAGINATE, "SearchController", PAGINATION_PARAM));
        } catch (Exception e) {
            e.printStackTrace();
            log(e.getMessage());
        }

        request.getRequestDispatcher(url).forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
