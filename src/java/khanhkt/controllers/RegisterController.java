/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.controllers;

import khanhkt.utils.MyConstants;
import khanhkt.utils.SendMailUtil;
import khanhkt.utils.VerifyRecaptchaUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import khanhkt.users.UsersDAO;
import khanhkt.users.UsersDTO;

/**
 *
 * @author CND
 */
@WebServlet(name = "RegisterController", urlPatterns = {"/RegisterController"})
public class RegisterController extends HttpServlet {

    private final String PAGE = "register.jsp";
    private final String PAGE_CONFIRM = "mail_confirmation.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPost)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = PAGE;
        request.setAttribute("siteKey", VerifyRecaptchaUtil.SITE_KEY);
        try {
            //Verify CAPTCHA.
            if (isPost) {
                UsersDAO user = new UsersDAO();
                String email = request.getParameter("email");
                String name = request.getParameter("name");
                String phone = request.getParameter("phone");
                String address = request.getParameter("address");
                String password = request.getParameter("password");
                if (!user.checkExistEmail(email)) {
                    UsersDTO userDto = user.register(new UsersDTO(email, password, name, phone, address, null, 0, 0));
                    
                    request.setAttribute("email", email);

                    url = PAGE_CONFIRM;

                } else {
                    url = PAGE;
                    request.setAttribute("email", email);
                    request.setAttribute("name", name);
                    request.setAttribute("phone", phone);
                    request.setAttribute("address", address);
                    request.setAttribute("password", password);
                    request.setAttribute("message", "<p style=\"color:red;\">Email is in used, please try again!!</p>");

                }

            } else {
                url = PAGE;
            }

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            log("Error Exception at " + this.getClass().getName() + ": " + e.getMessage());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, false);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, true);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
