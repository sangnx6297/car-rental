/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.controllers;

import khanhkt.utils.MyConstants;
import khanhkt.utils.SendMailUtil;
import khanhkt.utils.VerifyRecaptchaUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import khanhkt.users.UsersDAO;
import khanhkt.users.UsersDTO;
import khanhkt.utils.MailUtils;

/**
 *
 * @author CND
 */
@WebServlet(name = "VerificationController", urlPatterns = {"/VerificationController"})
public class VerificationController extends HttpServlet {

    private final String PAGE = "register.jsp";
    private final String PAGE_CONFIRM = "mail_confirmation.jsp";
    private final String PAGE_SEARCH = "/SearchController";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPost)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = PAGE;
        request.setAttribute("siteKey", VerifyRecaptchaUtil.SITE_KEY);
        try {
            //Verify CAPTCHA.
            if (isPost) {
                UsersDAO user = new UsersDAO();
                String email = request.getParameter("email");
                String code = request.getParameter("code");

                System.out.println("email" + email);
                UsersDTO usersDTO = user.verifyUser(email, code);

                if (usersDTO != null) {
                    url = PAGE_SEARCH;
                    HttpSession session = request.getSession();
                    session.setAttribute("user", usersDTO);
                    user.generateVerificationCode(email, "-1");
                    user.activeUser(email);
                } else {
                    request.setAttribute("message", "Invalid code !");
                    request.setAttribute("mail", email);
                    url = PAGE_CONFIRM;
                }

            } else {
                url = PAGE;
            }

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            log("Error Exception at " + this.getClass().getName() + ": " + e.getMessage());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, false);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, true);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
