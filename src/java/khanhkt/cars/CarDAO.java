/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.cars;

import com.mysql.jdbc.StringUtils;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.naming.NamingException;
import khanhkt.utils.DBHelper;

/**
 *
 * @author nhm95
 */
public class CarDAO implements Serializable {

    public List<CarDTO> list;
    public int total;

    private String sql = "SELECT c.*, ct.category_name "
            + "FROM cars c INNER JOIN category ct ON c.category_id = ct.id  ";

    private Connection con;
    private PreparedStatement stm;
    private ResultSet rs;

    public void getAll() throws NamingException, SQLException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {

                stm = con.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(new CarDTO(rs.getInt("id"), rs.getInt("category_id"),
                            rs.getString("code"), rs.getString("name"), rs.getFloat("price"),
                            rs.getString("year"), rs.getInt("quantity"), rs.getDate("rental_date"), rs.getString("category_name")));
                }
            }
        } finally {
            closeDB();
        }
    }

    public void getAll(String name, String category, String dateFrom, String dateTo, int limit, int ofset) throws NamingException, SQLException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String sql = this.sql;

                boolean haveFirtsCondistion = false;
                if (!StringUtils.isNullOrEmpty(category) && !StringUtils.isNullOrEmpty(name)) {
                    sql += " WHERE ( category_id = " + category + " OR name like '%" + name + "%'" + " )";
                    haveFirtsCondistion = true;
                } else if (!StringUtils.isNullOrEmpty(category)) {
                    sql += " WHERE category_id = " + category + " ";
                    haveFirtsCondistion = true;
                } else if (!StringUtils.isNullOrEmpty(name)) {
                    sql += " WHERE name like '%" + name + "%' ";
                    haveFirtsCondistion = true;
                } else {
                }

                if (!StringUtils.isNullOrEmpty(dateFrom) && !StringUtils.isNullOrEmpty(dateTo)) {
                    if (haveFirtsCondistion) {
                        sql += " AND rental_date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                    } else {
                        sql += " WHERE rental_date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                    }
                }

                sql += " ORDER BY name, year, category_id, price, quantity DESC LIMIT " + limit + " OFFSET " + ofset;

                System.out.println("query: " + sql);
                stm = con.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(new CarDTO(rs.getInt("id"), rs.getInt("category_id"),
                            rs.getString("code"), rs.getString("name"), rs.getFloat("price"),
                            rs.getString("year"), rs.getInt("quantity"), rs.getDate("rental_date"), rs.getString("category_name")));
                }
            }
        } finally {
            closeDB();
        }
    }

    public void getTotal(String name, String category, String dateFrom, String dateTo) throws NamingException, SQLException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {

                String sql = "SELECT count(*) as total "
                        + "FROM cars ";
                boolean haveFirtsCondistion = false;
                if (!StringUtils.isNullOrEmpty(category) && !StringUtils.isNullOrEmpty(name)) {
                    sql += " WHERE ( category_id = " + category + " OR name like '%" + name + "%'" + " )";
                    haveFirtsCondistion = true;
                } else if (!StringUtils.isNullOrEmpty(category)) {
                    sql += " WHERE category_id = " + category + " ";
                    haveFirtsCondistion = true;
                } else if (!StringUtils.isNullOrEmpty(name)) {
                    sql += " WHERE name like '%" + name + "%' ";
                    haveFirtsCondistion = true;
                } else {
                }

                if (!StringUtils.isNullOrEmpty(dateFrom) && !StringUtils.isNullOrEmpty(dateTo)) {
                    if (haveFirtsCondistion) {
                        sql += " AND rental_date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                    } else {
                        sql += " WHERE rental_date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                    }
                }

                stm = con.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    this.total = rs.getInt("total");
                }
            }
        } finally {
            closeDB();
        }
    }

    public void closeDB() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (stm != null) {
            stm.close();
        }
        if (con != null) {
            con.close();
        }
    }

}
