/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.cars;

import java.io.Serializable;
import java.sql.Date;
import khanhkt.utils.Utils;

/**
 *
 * @author nhm95
 */
public class CarDTO implements Serializable {

    private int id;
    private int categoryId;
    private String code;
    private String name;
    private float price;
    private String year;
    private int quantity;
    private Date rentalDate;
    private String categoryName;

    public CarDTO(int id, int categoryId, String code, String name, float price, String year, int quantity, Date rentalDate, String categoryName) {
        this.id = id;
        this.categoryId = categoryId;
        this.code = code;
        this.name = name;
        this.price = price;
        this.year = year;
        this.quantity = quantity;
        this.rentalDate = rentalDate;
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "CarDTO{" + "id=" + id + ", categoryId=" + categoryId + ", code=" + code + ", name=" + name + ", price=" + price + ", year=" + year + ", quantity=" + quantity + ", rental_date=" + rentalDate + '}';
    }

    public CarDTO(int categoryId, String code, String name, float price, String year, int quantity, Date rentalDate, String categoryName) {
        this.categoryId = categoryId;
        this.code = code;
        this.name = name;
        this.price = price;
        this.year = year;
        this.quantity = quantity;
        this.rentalDate = rentalDate;
        this.categoryName = categoryName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getRentalDate() {
        return rentalDate;
    }

    public void setRentalDate(Date rentalDate) {
        this.rentalDate = rentalDate;
    }

    public String getRentalDateString() {
        return Utils.formatDate(this.rentalDate);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    
}
