/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.users;

import khanhkt.utils.DBHelper;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import javax.naming.NamingException;
import khanhkt.utils.Utils;

/**
 *
 * @author CND
 */
public class UsersDAO implements Serializable {

    List<UsersDTO> list;
    private Connection con;
    private PreparedStatement stm;
    private ResultSet rs;

    public List<UsersDTO> getList() {
        return list;
    }

    private void closeDB() throws NamingException, SQLException {
        if (rs != null) {
            rs.close();
        }
        if (stm != null) {
            stm.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public UsersDTO checkLogin(String userID, String password) throws NamingException, SQLException {

        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String sql = "SELECT  id, email, password, name, phone, address, createDate, roleID, statusID "
                        + "FROM Users "
                        + "WHERE email = ? AND password = ?";
                stm = con.prepareStatement(sql);
                stm.setString(1, userID);
                stm.setString(2, password);
                rs = stm.executeQuery();
                if (rs.next()) {
                    return new UsersDTO(rs.getString("email"),
                            rs.getString("password"),
                            rs.getString("name"),
                            rs.getString("phone"),
                            rs.getString("address"),
                            rs.getDate("createDate"),
                            rs.getInt("roleID"),
                            rs.getInt("statusID"), rs.getInt("id"));
                }
            }
        } finally {
            closeDB();
        }

        return null;
    }

    public UsersDTO checkLoginGoogle(String email) throws NamingException, SQLException {

        con = DBHelper.getConnect();
        try {

        } finally {
            closeDB();
        }
        return null;
    }

    public boolean checkExistEmail(String email) throws NamingException, SQLException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String sql = "SELECT  email "
                        + " FROM Users "
                        + " WHERE email = ? ";
                stm = con.prepareStatement(sql);
                stm.setString(1, email);
                rs = stm.executeQuery();
                if (rs.next()) {
                    return true;
                }
            }
        } finally {
            closeDB();
        }

        return false;
    }

    public UsersDTO register(UsersDTO user) throws NamingException, SQLException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                PreparedStatement stmt = con.prepareStatement("insert into users (`email`, `password`, `status`, `name`, `phone`, `address`,`statusID`, `roleId`, `createDate`)"
                        + " values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                stmt.setString(1, user.getEmail());//1 specifies the first parameter in the query  
                stmt.setString(2, user.getPassword());
                stmt.setString(3, "A");
                stmt.setString(4, user.getName());
                stmt.setString(5, user.getPhone());

                stmt.setString(6, user.getAddress());
                stmt.setInt(7, 1);
                stmt.setInt(8, 2);
                stmt.setDate(9, new Date(timestamp.getTime()));
                int i = stmt.executeUpdate();
                if (i > 0) {
                    return checkLogin(user.getEmail(), user.getPassword());
                }
                System.out.println(i + " records inserted");
            }
        } finally {
            closeDB();
        }
        return user;
    }

    public String generateVerificationCode(String email, String code) throws NamingException, SQLException, NoSuchAlgorithmException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String md5 = Utils.md5(new java.util.Date().getTime() + "");
                if (code == null) {
                    code = md5.substring(0, 6).toUpperCase();
                }
                PreparedStatement stmt = con.prepareStatement("UPDATE  users SET verification_code = ? WHERE email = ?");
                stmt.setString(1, code);
                stmt.setString(2, email);//1 specifies the first parameter in the query  
                int i = stmt.executeUpdate();
                System.out.println(i + " records inserted");
                return code;
            }
        } finally {
            closeDB();
        }
        return null;
    }

    public UsersDTO findUser(String userID) throws NamingException, SQLException {

        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String sql = "SELECT  id, email, password, name, phone, address, createDate, roleID, statusID "
                        + "FROM Users "
                        + "WHERE email = ?";
                stm = con.prepareStatement(sql);
                stm.setString(1, userID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    return new UsersDTO(rs.getString("email"),
                            rs.getString("password"),
                            rs.getString("name"),
                            rs.getString("phone"),
                            rs.getString("address"),
                            rs.getDate("createDate"),
                            rs.getInt("roleID"),
                            rs.getInt("statusID"), rs.getInt("id"));
                }
            }
        } finally {
            closeDB();
        }

        return null;
    }

    public UsersDTO verifyUser(String userID, String code) throws NamingException, SQLException {

        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String sql = "SELECT  id, email, password, name, phone, address, createDate, roleID, statusID "
                        + "FROM Users "
                        + "WHERE email = ? AND verification_code = ?";
                stm = con.prepareStatement(sql);
                stm.setString(1, userID);
                stm.setString(2, code);
                rs = stm.executeQuery();
                if (rs.next()) {
                    return new UsersDTO(rs.getString("email"),
                            rs.getString("password"),
                            rs.getString("name"),
                            rs.getString("phone"),
                            rs.getString("address"),
                            rs.getDate("createDate"),
                            rs.getInt("roleID"),
                            rs.getInt("statusID"), rs.getInt("id"));
                }
            }
        } finally {
            closeDB();
        }

        return null;
    }

    public void activeUser(String email) throws NamingException, SQLException, NoSuchAlgorithmException {
        con = DBHelper.getConnect();
        try {
            if (con != null) {
                String md5 = Utils.md5(new java.util.Date().getTime() + "");
                String code = md5.substring(0, 6).toUpperCase();
                PreparedStatement stmt = con.prepareStatement("UPDATE  users SET statusID = 2 WHERE email = ?");
                stmt.setString(1, email);//1 specifies the first parameter in the query  
                int i = stmt.executeUpdate();
                System.out.println(i + " records inserted");
            }
        } finally {
            closeDB();
        }
    }
}
