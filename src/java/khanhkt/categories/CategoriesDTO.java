/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.categories;

import java.io.Serializable;

/**
 *
 * @author CND
 */
public class CategoriesDTO implements Serializable {

    private int id;
    private String name;

    public CategoriesDTO() {
    }

    public CategoriesDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
