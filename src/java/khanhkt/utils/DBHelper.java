/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.utils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author CND
 */
public class DBHelper implements Serializable {

    public static Connection getConnect() throws NamingException, SQLException {
//        Context context = new InitialContext();
//        Context tomcatContext = (Context) context.lookup("java:comp/env");
//        DataSource ds = (DataSource) tomcatContext.lookup("ResourceSharing");    
//        return ds.getConnection();

 try {

            // Get each property value.
            String dbDriverClass = "com.mysql.jdbc.Driver";

            String dbConnUrl = "jdbc:mysql://localhost:3306/phat_project";

            String dbUserName = "root";

            String dbPassword = "";

            if (!"".equals(dbDriverClass) && !"".equals(dbConnUrl)) {
                /* Register jdbc driver class. */
                Class.forName(dbDriverClass);

                // Get database connection object.
                Connection dbConn = DriverManager.getConnection(dbConnUrl, dbUserName, dbPassword);

                // Get dtabase meta data.
                DatabaseMetaData dbMetaData = dbConn.getMetaData();

                // Get database name.
                String dbName = dbMetaData.getDatabaseProductName();

                // Get database version.
                String dbVersion = dbMetaData.getDatabaseProductVersion();

//                System.out.println("Database Name : " + dbName);

//                System.out.println("Database Version : " + dbVersion);
                return dbConn;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
