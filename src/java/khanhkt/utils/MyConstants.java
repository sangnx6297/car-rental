/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.utils;

import java.io.Serializable;

/**
 *
 * @author CND
 */
public class MyConstants implements Serializable {

    //paging
    public static final int TOTAL_ITEM_IN_PAGE = 20;
    //Role
    public static final int ROLE_EMPLOYEE = 2,
            ROLE_MANAGE = 1;

    //Status
    public static final int STATUS_NEW = 1,
            STATUS_ACTIVE = 2,
            STATUS_INACTIVE = 3,
            STATUS_ACCEPT = 4,
            STATUS_DELETE = 5;

    //Recaptcha google
    //OAuth Google
    //Send Email
    // limit pages
    public static final int LIMIT_PAGINATE = 20;

    public static final String PATTERN_DATE_FORMAT = "YYYY-MM-dd";

    public static final String MAIL_SERVER_USERNAME = "sevemail7@gmail.com";
    public static final String MAIL_SERVER_PASS = "sevemail@123";
    public static final String MAIL_SERVER_PORT = "456";
    public static final String MAIL_SERVER_HOST = "smtp.gmail.com";
    public static final String MAIL_CONFIRMATION_SUBJECT = "CARS RENTAL VERIFICATION CODE";

}
