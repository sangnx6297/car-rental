/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khanhkt.utils;

import com.mysql.jdbc.StringUtils;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nhm95
 */
public class Utils {

    public static int convertPageToOfsset(int limit, int page) {
        int offset = 0;

        if (page > 1) {

            offset = (page * limit) - limit;

        }

        return offset;
    }

    public static String generatePagination(String queryParam, int total, int limit, String url, String params) {
        String html = "";
        float page = total / limit;
        int value = Math.round(page);

        if (page < 1) {
            return html;
        } else {
            if (total % limit != 0) {
                value++;
            }
        }

        if (!StringUtils.isNullOrEmpty(queryParam)) {
            queryParam = queryParam.replaceAll("&" + params + "=[0-9]{0,99999}", "");
        }

        for (int i = 1; i <= value; i++) {
            html += "<a href=\"" + url + "?" + queryParam + "&" + params + "=" + i + "\">" + i + "</a>";
        }

        return html;
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat(MyConstants.PATTERN_DATE_FORMAT).format(date);
    }

    public static String md5(String string) {
        try {
//            String s = new String(new Date().getTime() + "");
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(string.getBytes(), 0, string.length());
            return new BigInteger(1, m.digest()).toString(16);
//            MessageDigest m = MessageDigest.getInstance("MD5");
//            m.update(new String(new Date().getTime() + "").getBytes());
//            System.out.println(new String(m.digest()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
