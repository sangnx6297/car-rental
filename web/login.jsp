<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- reCAPTCHA with English language -->
        <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>


        <!-- reCAPTCHA with Vietnamese language -->
        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>


        <!-- reCAPTCHA with Auto language -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script>
//            window.onload = function () {
//                var $recaptcha = document.querySelector('#g-recaptcha-response');
//
//                if ($recaptcha) {
//                    $recaptcha.setAttribute("required", "required");
//                }
//            };
        </script>
    </head>
    <body>
        <h1>Login Page</h1>
        <p style="color:red;">${requestScope.errorString}</p>

        <form action="LoginController" method="POST">
            Username <input type="text" name="userID" value="sangnx" /><br/> <br/>
            Password <input type="password" name="password" value="sangnx" /><br/> <br/>

            <div class="g-recaptcha" data-sitekey="${requestScope.siteKey}"></div>
            <input type="submit" value="Login" name="btAction" />
            <input type="reset" value="Reset" />
        </form>
    </body>
</html>
