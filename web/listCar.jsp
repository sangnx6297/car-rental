<%-- 
    Document   : cars
    Created on : Jan 16, 2022, 3:57:42 PM
    Author     : nhm95
--%>

<%@page import="khanhkt.cars.CarDAO"%>
<%@page import="java.util.List"%>
<%@page import="khanhkt.cars.CarDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List Car</title>
    </head>
    <body>
        <div class="nav">

            <c:if  test="${sessionScope.user != null}">
                <form method="post" action="LogoutController" id="logout-form">
                    <c:out value="${sessionScope.user.email}"/>  <a href="#" onclick="this.parentNode.submit();" method="post">Logout</a>
                </form>
            </c:if>

            <c:if  test="${sessionScope.user == null}">
                <a href="LoginController" >Login</a>
                &nbsp;
                <a href="RegisterController" >Register</a>

            </c:if>

        </div>
        <div class="search-container">
            <form action="SearchController" method="GET"> 
                Name <input type="text" name="carName" value="${requestScope.carName}" /><br/>
                Category <select name="carCategory">
                    <option value="">N/A</option>
                    <c:forEach items="${requestScope.listCategory}" var="category">
                        <c:if test="${category.id == requestScope.carCategory}">
                            <option value="${category.id}" selected>${category.name}</option>
                        </c:if>
                        <c:if test="${category.id != requestScope.carCategory}">
                            <option value="${category.id}">${category.name}</option>
                        </c:if>

                    </c:forEach>
                </select><br/>
                Rental Date <input type="text" name="rentalDateFrom" value="${requestScope.rentalDateFrom}" placeholder="From"/> 
                &nbsp; <input type="text" name="rentalDateTo" value="${requestScope.rentalDateTo}" placeholder="To"/> <br>
                <input type="submit" value="search" name="btnSearch" />
                <input type="reset" value="Reset" />
            </form>
        </div>
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Year</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Rental Date</th>
                </tr>
            </thead>
            <c:set var="index" scope="request" value="0"/>
            <c:forEach items="${requestScope.list}" var="car">
                <c:set var="index" scope="request" value="${index + 1}"/>
                <tr>
                    <td><c:out value="${index}"/></td>
                    <td><c:out value="${car.code}" /></td>
                    <td><c:out value="${car.name}" /></td>
                    <td><c:out value="${car.year}" /></td>
                    <td><c:out value="${car.categoryName}" /></td>
                    <td><c:out value="${car.price}" /></td>
                    <td><c:out value="${car.rentalDateString}" /></td>
                </tr>
            </c:forEach>
        </table>
        ${requestScope.pagination}    
    </body>
</html>
