<%-- 
    Document   : feedback
    Created on : Jun 25, 2018, 2:59:24 PM
    Author     : THAINDSE62642
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Feedback</title>
    </head>
    <body>
        <h1>Feedback Form</h1>
        <font color="red">
        Welcome, ${sessionScope.FULLNAME}
        </font>, <a href="Logout">Logout</a><br/>
        Student Id: ${sessionScope.GETUSERNAME} <br/>
        Student Name: ${sessionScope.FULLNAME}<br/>
        <br/>
        <br/>
        Some marks of courses is not correct. Please, explain them for me
        <c:set var="result" value="${requestScope.FEEDBACK}"/>
        <c:if test="${not empty result}">
            <form action="Delete">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Avg</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="dto" items="${result}" varStatus="counter">
                            <tr>
                                <td>${counter.count}</td>
                                <td>${dto.subjectID}</td>
                                <td>${dto.subjectName}
                                    <input type="hidden" name="subName" value="${dto.subjectName}" />
                                </td>
                                <td>${dto.subjectAvg}</td>
                                <td>${dto.status}</td>
                                <td>
                                    <input type="checkbox" name="checkid" value="${dto.id}" />
                                    <input type="hidden" name="markid" value="${dto.id}"/>
                                </td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="5">
                                <input type="submit" value="Send" name ="button"/>
                            </td>
                            <td >
                                <input type="submit" value="Remove" name="button"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <br/><a href="View?txtUsername=${sessionScope.GETUSERNAME}" >Marks Table</a><br/>
        </c:if>
        <c:if test="${empty result}">
            <br/>List feedback null<br/>
            Please <a href="View?txtUsername=${sessionScope.GETUSERNAME}">try again </a> !
        </c:if>
        
    </body>
</html>
