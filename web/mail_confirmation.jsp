<%-- 
    Document   : mail_confirmation
    Created on : Jan 18, 2022, 11:44:15 PM
    Author     : nhm95
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Verification Code</h1>
        <p>A verification code has been sent to ${requestScope.email}</p>
        <p>
        <form method="post" action="ReSentVerificationCodeController">
            <input type="hidden" name="email" value="${requestScope.email}"/>
            <a onclick="this.parentNode.submit()" href="#">Didn't you receive a code?</a>
        </form>
    </p>
    <form method="post" action="VerificationController">
        <label>Code</label>&nbsp;<input type="text" value="" name="code"> <br>
        <input type="hidden" name="email" value="${requestScope.email}"/>
        <input type="submit" value="Continue">
    </form>
    <p style="color: red;">${requestScope.message}</p>   
</body>
</html>
