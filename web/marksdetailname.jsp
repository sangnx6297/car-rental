<%-- 
    Document   : marksdetailname
    Created on : Jun 26, 2018, 9:02:15 AM
    Author     : THAINDSE62642
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Marks Detail</title>
    </head>
    <body>
        <font color="red">
        Welcome, ${sessionScope.FULLNAME}
        </font>, <a href="Logout">Logout</a>
        <h1>Subjects' mark details</h1>
        <p>Subject Name: ${sessionScope.SUBJECTNAME}</p>
        <c:set var="result" value="${sessionScope.MARKSDETAILNAME}"/>
            <table border="1">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Subject Name</th>
                        <th>Block</th>
                        <th>Semester</th>
                        <th>Year</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dto" items="${result}" varStatus="counter">
                    <tr>
                        <td>${counter.count}</td>
                        <td>${dto.subjectName}</td>
                        <td>${dto.block}</td>
                        <td>${dto.semester}</td>
                        <td>${dto.year}</td>
                        <td>${dto.status}</td>
                    </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="5">
                            Number of Studying: ${sessionScope.SUMCOUNT}
                        </td>
                    </tr>
                </tbody>
            </table>
        <a href="View?txtUsername=${sessionScope.GETUSERNAME}" >Marks Table</a>
    </body>
</html>
