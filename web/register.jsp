<%-- 
    Document   : register
    Created on : Jan 18, 2022, 10:38:11 PM
    Author     : nhm95
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>
        <style>
            .required::after{
                content: " *";
                color: red;
            }
        </style>
        <h1>Register</h1>
        <form method="POST" onsubmit="return checkMatch()" action="RegisterController">
            <label class="required">Email:</label>&nbsp;<input type="email" name="email" required value="${requestScope.email}"></br>
            <label>Name:</label>&nbsp;<input type="text" name="name" value="${requestScope.name}"></br>
            <label>Phone:</label>&nbsp;<input type="tel" name="phone" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" value="${requestScope.phone}"></br>
            <label>Address:</label>&nbsp;<input type="text" name="address" value="${requestScope.address}"></br>
            <label  class="required">Password:</label>&nbsp;<input type="password" name="password" required id="password"></br>
            <label  class="required">Re-Password</label>&nbsp;<input type="password" name="re-password" required id="pre-password" onchange="checkMatch()"></br>
            <input type="submit" value="Submit" onsubmit="return false;">
            <div id="clearfix">
               ${requestScope.message}
            </div>
        </form>
    </body>
    <script type="text/javascript">
        var timeout = null;

<c:if test="${requestScope.message != null}">
timeout = setTimeout(function(){
                var clearfix = document.getElementById('clearfix');
                clearfix.innerHTML = '';
    }, 3000);
    </c:if>
        function checkMatch() {
            var password = document.getElementById('password');
            var pre_password = document.getElementById('pre-password');

            if (password.value != (pre_password.value)) {
                var clearfix = document.getElementById('clearfix');
                clearfix.innerHTML = '<p style="color:red;">Your pre-password is miss match, try again!!</p>';
                if (timeout) {
                    clearTimeout(timeout);
                }

                timeout = setTimeout(function () {
                    clearfix.innerHTML = '';
                }, 3000);
                return false;
            }
            return true;
        }
                </script>
                </html>
