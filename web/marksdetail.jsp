<%-- 
    Document   : marksdetail
    Created on : Jun 24, 2018, 12:53:39 AM
    Author     : THAINDSE62642
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Marks Detail</title>
    </head>
    <body>
        <font color="red">
        Welcome, ${sessionScope.FULLNAME}
        </font>, <a href="Logout">Logout</a>
        <h1>Mark Table</h1>
        <p>Subjects' mark details</p>
        <c:set var="mresult" value="${requestScope.MARKSDETAIL}"/>
        <c:if test="${not empty mresult}">
            <form action="Feedback">
            
            <table border="1">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Subject Name</th>
                        <th>Block</th>
                        <th>Semester</th>
                        <th>Year</th>
                        <th>Avg</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dto" items="${mresult}" varStatus="counter">
                    
                        <tr>
                            <td>${counter.count}</td>
                            <td>${dto.subjectName}
                                
                            </td>
                            <td>${dto.block}</td>
                            <td>${dto.semester}</td>
                            <td>${dto.year}</td>
                            <td>${dto.subjectAvg}</td>
                            <td>${dto.status}</td>
                            <td>
                                
                                <a href="ViewDetail?id=${dto.subjectID}">View Detail</a>
                            </td>
                            <td>
                                <input type="checkbox" name="chk" value="${dto.subjectID}" />
                            </td>
                        </tr>
                        
                    </c:forEach>
                        <tr>
                            <td colspan="4">
                                Pass Credits: ${sessionScope.PASSCREDITS}
                            </td>
                            <td colspan="3">
                                GPA: ${sessionScope.GPA}
                            </td>
                            <td colspan="2">
                                <input type="submit" value="Send Feedback" />
                            </td>
                        </tr>
                </tbody>
            </table>
            </form>
                            <a href="View?txtUsername=${sessionScope.GETUSERNAME}" >Marks Table</a>
        </c:if>
        <c:if test="${empty mresult}">
            <h1>No data to print !!!</h1>
        </c:if>
            
    </body>
</html>
